//
//  UIView+GameView.h
//  p03-pradhan
//
//  Created by Nimesh on 2/17/17.
//  Copyright © 2017 npradha2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Jumper.h"
#import "Brick.h"

@interface GameView : UIView {
    
}
@property (nonatomic, strong) Jumper *jumper;
@property (nonatomic, strong) NSMutableArray *bricks;
@property (nonatomic) float tilt;
-(void)arrange:(CADisplayLink *)sender;

@end
